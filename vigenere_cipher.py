import string


class VigenereCipher(object):
    """Encode and decode text with cipher key and alphabet"""
    def __init__(self, key, alphabet):
        """Constructor"""
        self.key = key
        self.alphabet = alphabet

    def password_length(self, text):
        """Get password of the length appropriate to the text"""
        if len(self.key) >= len(text):
            password = self.key[:len(text)]
        else:
            password = self.key
            while len(password) <= len(text):
                password += password
            if len(password) > len(text):
                password = password[:len(text)]
        return password

    def encode(self, text):
        """Encode text"""
        letter_encoded = []
        password = self.password_length(text)
        index_of_letter = 0

        for i in text:
            if i in self.alphabet:
                text_char_index = self.alphabet.index(i)
                password_char_index = self.alphabet.index(password[index_of_letter])
                cipher = (text_char_index + password_char_index) % len(self.alphabet)
                letter_encoded.append(self.alphabet[cipher])
            else:
                letter_encoded.append(i)
            index_of_letter += 1
        return "".join(letter_encoded)

    def decode(self, text):
        """Decode text"""
        letter_decoded = []
        password = self.password_length(text)
        index_of_letter = 0
        for i in text:
            if i in self.alphabet:
                password_char_index = self.alphabet.index(password[index_of_letter])
                cipher = self.alphabet.index(i)
                text_char_index = (cipher - password_char_index) % len(self.alphabet)
                letter_decoded.append(self.alphabet[text_char_index])
            else:
                letter_decoded.append(i)
            index_of_letter += 1
        return "".join(letter_decoded)


a = VigenereCipher("password", string.ascii_lowercase)
print(a.encode('Test phrase!'))
print(a.decode(a.encode('Test phrase!')))
